import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { render, fireEvent } from '@testing-library/react';

import Switcher from '../Switcher';

test('Switcher组件通过点击按钮显示开／关', () => {
  // <--start
  // TODO 4: 给出正确的测试
  const { getByTestId } = render(<Switcher />);
  const button = getByTestId('switch-button');

  expect(button).toHaveTextContent('开');

  fireEvent.click(button);

  const afterClickButton = getByTestId('switch-button');

  expect(afterClickButton).toHaveTextContent('关');
  // --end->
});
