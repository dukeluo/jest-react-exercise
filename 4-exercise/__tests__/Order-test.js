import React from 'react';

import '@testing-library/jest-dom/extend-expect';
import '@testing-library/react/cleanup-after-each';

import { render, fireEvent } from '@testing-library/react';

import axios from 'axios';
import 'babel-polyfill';

import OrderComponent from '../Order';

jest.mock('axios'); // Mock axios模块

test('Order组件显示异步调用订单数据', async () => {
  // <--start
  // TODO 4: 给出正确的测试

  // setup组件
  const { findByTestId, getByLabelText } = render(<OrderComponent />);
  const button = getByLabelText('submit-button');
  const input = getByLabelText('number-input');
  const status = findByTestId('status');

  // Mock数据请求
  const resp = { data: { status: '已完成' } };
  axios.get.mockResolvedValue(resp);

  // 触发事件
  fireEvent.change(input, { target: { value: '1666' } });
  fireEvent.click(button);

  // 给出断言
  expect(axios.get).toHaveBeenCalledWith('/order/1666');
  expect(input.value).toBe('1666');
  // await expect(status).resolves.toHaveTextContent('已完成');
  return expect(status).resolves.toHaveTextContent('已完成');
  // --end->
});
